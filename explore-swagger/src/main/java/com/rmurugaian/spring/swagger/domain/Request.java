package com.rmurugaian.spring.swagger.domain;

/**
 * @author rmurugaian 2019-02-20
 */
public class Request {

    private String name;
    private String age;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(final String age) {
        this.age = age;
    }
}
