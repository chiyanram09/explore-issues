package com.rmurugaian.spring.swagger.api;

import com.rmurugaian.spring.swagger.domain.Request;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import jdk.nashorn.internal.ir.ObjectNode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author rmurugaian 2019-02-19
 */
@RestController
public class SwaggerApiController {


    @Value("${api.tokenSecret:${api.token}}")
    private String token;

    @PostMapping("/name")
    public Request swagger(
        @ApiParam(value = "json", example = "{\n"
            + "  \"name\": \"vijay\",\n"
            + "  \"age\": 23\n"
            + "}") @RequestBody final Request request) {

        return request;
    }

    @PostMapping("/objectNode")
    public ObjectNode swaggerJsonNode(
        @ApiParam(value = "json", examples = @Example(value = @ExampleProperty(value = "{\n"
            + "  \"name\": \"vijay\",\n"
            + "  \"age\": 23\n"
            + "}")), format = "json",type = "") @RequestBody final ObjectNode objectNode) {

        return objectNode;
    }
}

