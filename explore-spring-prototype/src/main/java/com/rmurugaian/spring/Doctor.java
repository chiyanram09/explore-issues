package com.rmurugaian.spring;

/**
 * @author rmurugaian 2019-04-05
 */
public class Doctor {

    private String name;
    private String position;

    public Doctor(final String name, final String position) {
        this.name = name;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }
}
