package com.rmurugaian.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author rmurugaian 2019-04-05
 */
public class PrototypeValidator {

    public static void main(String[] args) {

        try (final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(SpringConfig.class)) {
            final SpringConfig contextBean = context.getBean(SpringConfig.class);
            contextBean.print();

            final Doctor doctor = (Doctor) context.getBean("doctor");
            System.out.println(doctor.getName() + " ::: " + doctor.getPosition());

        } finally {
            System.out.println("All Done ******* ");
        }
    }
}
