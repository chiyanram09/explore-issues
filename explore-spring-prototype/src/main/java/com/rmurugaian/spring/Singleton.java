package com.rmurugaian.spring;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author rmurugaian 2019-04-05
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class Singleton {

    @Resource
    private Prototype prototype;

    public Prototype getPrototype() {
        return prototype;
    }
}
