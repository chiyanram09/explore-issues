package com.rmurugaian.spring;

import org.springframework.web.client.RestTemplate;

/**
 * @author rmurugaian 2019-01-04
 */
public class SpringCallController {

    public static void main(String[] args) {
        final RestTemplate restTemplate = new RestTemplate();

        restTemplate.getForObject("http://localhost:8080/errorCase", String.class);
    }

}
